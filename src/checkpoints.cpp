// Copyright (c) 2009-2012 The Bitcoin developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <boost/assign/list_of.hpp> // for 'map_list_of()'
#include <boost/foreach.hpp>

#include "checkpoints.h"

#include "main.h"
#include "uint256.h"

namespace Checkpoints
{
    typedef std::map<int, uint256> MapCheckpoints;

    // How many times we expect transactions after the last checkpoint to
    // be slower. This number is a compromise, as it can't be accurate for
    // every system. When reindexing from a fast disk with a slow CPU, it
    // can be up to 20, while when downloading from a slow network with a
    // fast multicore CPU, it won't be much higher than 1.
    static const double fSigcheckVerificationFactor = 5.0;

    struct CCheckpointData {
        const MapCheckpoints *mapCheckpoints;
        int64 nTimeLastCheckpoint;
        int64 nTransactionsLastCheckpoint;
        double fTransactionsPerDay;
    };

    // What makes a good checkpoint block?
    // + Is surrounded by blocks with reasonable timestamps
    //   (no blocks before with a timestamp after, none after with
    //    timestamp before)
    // + Contains no strange transactions
    static MapCheckpoints mapCheckpoints =
        boost::assign::map_list_of
     (  0, uint256("0x000003a02ae95396c8611d59ecdd2fe741d3bfa56aa3eff16b0a5032977bf794"))
	(	3, uint256("0x000006b0e119797d5119894c59d714a3a01079e9702e2479c63fc8df7c06d546"))
	(	4, uint256("0x000000e9f03017423caf71fbac8bbd6ca5a92b50b3f7c557cf8e6615a05664c7"))
	(	500, uint256("0x0000000002ac885f1a6de3594cc4a72c7cb6391cc33de702889d23f8daae8994"))
	(	35500, uint256("0x0000000037f2af1a17c1524093aa142da8cab0b764896cffdfa2ec2b09033fd6"))
	(	73930, uint256("0x000000094d12d0ac772ca5916c429199c4cd761a067bf560355cb8997c4dbeb2"))
	(	100000, uint256("0x00000007541bafd341eff316ee1e390dd617f0dce7905c6af979be0397faf4ed"))
	(	166392, uint256("0x00000008357c21f4e97e593dd3cab3d8b7d15d4cded570d2279545f7a0265816"))
	(	200000, uint256("0x000000007ab8e6e749b4d9d882b1e84aebcc883d8663e938ce4eed74fbda0254"))
	(	250000, uint256("0x00000003cc107059bc17175e8dac82a383125f096df97258b6e5ba767e458476"))
	(	299999, uint256("0x00000000b3576b3e819634f0a4202b32926ca5358fc8d431e51e7fcb79c35b07"))
	(	304959, uint256("0x00000003a3c2df01b1d939791cec843c2e8230d879bb20e4e45cc39152e690b6"))	
	(	350000, uint256("0x00000004d0b414fcc10378d1aef92ee6d5d2aa8e9613340db727d84609843b2a"))	
	(	400000, uint256("0x00000000efe6dc8721825a6ace8a24eaee38e9eabba50fd77ffa7c4077586b5e"))	
	(	450000, uint256("0x000000004fabfaf68249786f1a3f0c26d4c0f76090a27fdd7ae71472db175362"))	
	(	500000, uint256("0x000000032498aec05675281287faf0621df66802a167a9456c9fb0e7e8b45ec6"))	
	(	550000, uint256("0x0000000174c8d1c062a2b639f36e462f0c32bf1149cb312c82c2a2ed70e40b64"))	
	    ;
		
    static const CCheckpointData data = {
        &mapCheckpoints,
        1393373461, // * UNIX timestamp of last checkpoint block
        25000,    // * total number of transactions between genesis and last checkpoint
                    //   (the tx=... number in the SetBestChain debug.log lines)
        800.0     // * estimated number of transactions per day after checkpoint
    };

    static MapCheckpoints mapCheckpointsTestnet = 
        boost::assign::map_list_of
        (  0, uint256("0x00000c0cba168f821435122dec2e41ca812ff79f9bbdb6605d550b92a350b235"))
		(  10000, uint256("0x00000735028e2b3ceefdb62968c7525d5cfc2d76b3e56a82179bd98057340848"))
		(  20000, uint256("0x00000d2ed87e9514d82a5ec71020e2eec2800f7e18431b5a995b692348a3fae8"))
        ;
    static const CCheckpointData dataTestnet = {
        &mapCheckpointsTestnet,
        1393373461,
        3000,
        30
    };

    const CCheckpointData &Checkpoints() {
        if (fTestNet)
            return dataTestnet;
        else
            return data;
    }

    bool CheckBlock(int nHeight, const uint256& hash)
    {
        if (fTestNet) return true; // Testnet has no checkpoints
        if (!GetBoolArg("-checkpoints", true))
            return true;

        const MapCheckpoints& checkpoints = *Checkpoints().mapCheckpoints;

        MapCheckpoints::const_iterator i = checkpoints.find(nHeight);
        if (i == checkpoints.end()) return true;
        return hash == i->second;
    }

    // Guess how far we are in the verification process at the given block index
    double GuessVerificationProgress(CBlockIndex *pindex) {
        if (pindex==NULL)
            return 0.0;

        int64 nNow = time(NULL);

        double fWorkBefore = 0.0; // Amount of work done before pindex
        double fWorkAfter = 0.0;  // Amount of work left after pindex (estimated)
        // Work is defined as: 1.0 per transaction before the last checkoint, and
        // fSigcheckVerificationFactor per transaction after.

        const CCheckpointData &data = Checkpoints();

        if (pindex->nChainTx <= data.nTransactionsLastCheckpoint) {
            double nCheapBefore = pindex->nChainTx;
            double nCheapAfter = data.nTransactionsLastCheckpoint - pindex->nChainTx;
            double nExpensiveAfter = (nNow - data.nTimeLastCheckpoint)/86400.0*data.fTransactionsPerDay;
            fWorkBefore = nCheapBefore;
            fWorkAfter = nCheapAfter + nExpensiveAfter*fSigcheckVerificationFactor;
        } else {
            double nCheapBefore = data.nTransactionsLastCheckpoint;
            double nExpensiveBefore = pindex->nChainTx - data.nTransactionsLastCheckpoint;
            double nExpensiveAfter = (nNow - pindex->nTime)/86400.0*data.fTransactionsPerDay;
            fWorkBefore = nCheapBefore + nExpensiveBefore*fSigcheckVerificationFactor;
            fWorkAfter = nExpensiveAfter*fSigcheckVerificationFactor;
        }

        return fWorkBefore / (fWorkBefore + fWorkAfter);
    }

    int GetTotalBlocksEstimate()
    {
        if (fTestNet) return 0; // Testnet has no checkpoints
        if (!GetBoolArg("-checkpoints", true))
            return 0;

        const MapCheckpoints& checkpoints = *Checkpoints().mapCheckpoints;

        return checkpoints.rbegin()->first;
    }

    CBlockIndex* GetLastCheckpoint(const std::map<uint256, CBlockIndex*>& mapBlockIndex)
    {
        if (fTestNet) return NULL; // Testnet has no checkpoints
        if (!GetBoolArg("-checkpoints", true))
            return NULL;

        const MapCheckpoints& checkpoints = *Checkpoints().mapCheckpoints;

        BOOST_REVERSE_FOREACH(const MapCheckpoints::value_type& i, checkpoints)
        {
            const uint256& hash = i.second;
            std::map<uint256, CBlockIndex*>::const_iterator t = mapBlockIndex.find(hash);
            if (t != mapBlockIndex.end())
                return t->second;
        }
        return NULL;
    }
}
